const app = angular.module('app', [
    'ui.router'
])

// routing

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/questions');

    $stateProvider


        .state('questions', {
            url: '/questions',
            templateUrl: 'javascripts/src/views/questions.html',
            controller: 'questionsCtrl'
        })


        .state('articles', {
            url: '/articles',
            templateUrl: 'javascripts/src/views/articles.html'
        })

        .state('videos', {
            url: '/videos',
            templateUrl: 'javascripts/src/views/videos.html'
        })
});


