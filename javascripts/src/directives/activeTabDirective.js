/*
*   Directive to toggle active class if ui-sref matches current state
*
* */
app.directive('activeTab', [ '$transitions', function($transitions){
    return {
        restrict: 'A',
        link: function(scope, el, attr, ctrl) {

            $transitions.onSuccess({}, function($transition$) {

                $transition$.to().name == attr.uiSref ?
                    el.addClass('active')             :
                    el.removeClass('active')

            });
        }
    }
}])
