Test project for Nir

To run project execute:


1. Clone project
```
git clone https://Alexand3rSergan@bitbucket.org/Alexand3rSergan/js-test.git
```

2. Install dependencies
```
npm i
```

3. Install livereload
```
npm install -g livereload
```

4. Run the project
```
npm start
```